-- Пример 1: Добавление клиента с id=1, именем "John" и возрастом 30
INSERT INTO customer (id, name, age) VALUES (1, 'John', 30);

-- Пример 2: Добавление клиента с id=2, именем "Alice" и возрастом 25
INSERT INTO customer (id, name, age) VALUES (2, 'Alice', 25);

-- Пример 3: Добавление клиента с id=3, именем "Bob" и возрастом 35
INSERT INTO customer (id, name, age) VALUES (3, 'Bob', 35);

-- Пример 4: Добавление клиента с id=4, именем "Emily" и возрастом 28
INSERT INTO customer (id, name, age) VALUES (4, 'Emily', 28);

-- Пример 5: Добавление клиента с id=5, именем "David" и возрастом 40
INSERT INTO customer (id, name, age) VALUES (5, 'David', 40);

-- Пример 6: Добавление клиента с id=6, именем "Sarah" и возрастом 22
INSERT INTO customer (id, name, age) VALUES (6, 'Sarah', 22);

-- Пример 7: Добавление клиента с id=7, именем "Michael" и возрастом 33
INSERT INTO customer (id, name, age) VALUES (7, 'Michael', 33);

-- Пример 8: Добавление клиента с id=8, именем "Olivia" и возрастом 29
INSERT INTO customer (id, name, age) VALUES (8, 'Olivia', 29);

-- Пример 9: Добавление клиента с id=9, именем "Sophia" и возрастом 27
INSERT INTO customer (id, name, age) VALUES (9, 'Sophia', 27);

-- Пример 10: Добавление клиента с id=10, именем "Daniel" и возрастом 31
INSERT INTO customer (id, name, age) VALUES (10, 'Daniel', 31);