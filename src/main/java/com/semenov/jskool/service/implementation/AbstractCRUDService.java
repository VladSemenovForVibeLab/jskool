package com.semenov.jskool.service.implementation;

import com.semenov.jskool.mappers.Mappable;
import com.semenov.jskool.service.interf.CRUDService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCRUDService<E, K, T> implements CRUDService<E, K, T> {
    abstract JpaRepository<E, K> getRepository();

    abstract Mappable<E, T> getMappable();

    @Override
    public void add(T entity) {
        try {
            E entityCreated = getMappable().toEntity(entity);
            getRepository().save(entityCreated);
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

    @Override
    public T findById(K id) {
        E entity = getRepository().findById(id).orElse(null);
        T entityDto = getMappable().toDto(entity);
        return entityDto;
    }

    @Override
    public List<T> findAll() {
        List<E> objects = new ArrayList<>();
        getRepository().findAll().forEach(objects::add);
        List<T> objectsDto = getMappable().toDto(objects);
        return objectsDto;
    }

    @Override
    public E update(E entity) {
        getRepository().save(entity);
       return entity;
    }

    @Override
    public void delete(K id) {
        E entityFind = getRepository().findById(id).orElse(null);
        getRepository().delete(entityFind);
    }
}
