package com.semenov.jskool.service.implementation;

import com.semenov.jskool.entity.Customer;
import com.semenov.jskool.mappers.CustomerMapper;
import com.semenov.jskool.mappers.Mappable;
import com.semenov.jskool.payload.CustomerRequest;
import com.semenov.jskool.repository.CustomerRepository;
import com.semenov.jskool.service.interf.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerServiceImplementation extends AbstractCRUDService<Customer,Long,CustomerRequest> implements CustomerService {
   private final CustomerRepository customerRepository;
   private final CustomerMapper customerMapper;
    @Override
    JpaRepository<Customer, Long> getRepository() {
        return customerRepository;
    }

    @Override
    Mappable<Customer, CustomerRequest> getMappable() {
        return customerMapper;
    }
}
