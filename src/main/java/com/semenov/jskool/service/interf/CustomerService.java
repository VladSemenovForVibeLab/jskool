package com.semenov.jskool.service.interf;

import com.semenov.jskool.entity.Customer;
import com.semenov.jskool.payload.CustomerRequest;

public interface CustomerService extends CRUDService<Customer,Long,CustomerRequest> {

}
