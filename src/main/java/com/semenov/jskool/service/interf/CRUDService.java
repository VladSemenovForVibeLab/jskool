package com.semenov.jskool.service.interf;

import java.util.List;

public interface CRUDService <E,K,T>{
    void add(T entity);
    T findById(K id);
    List<T> findAll();
    E update(E entity);
    void delete(K id);
}
