package com.semenov.jskool.mappers;

import com.semenov.jskool.entity.Customer;
import com.semenov.jskool.payload.CustomerRequest;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends Mappable<Customer, CustomerRequest> {

    @Autowired
    Mappable<Customer, CustomerRequest> mappable = new Mappable<Customer, CustomerRequest>() {
        @Override
        public CustomerRequest toDto(Customer entity) {
            return new CustomerRequest(entity.getName(), entity.getAge());
        }

        @Override
        public List<CustomerRequest> toDto(List<Customer> entities) {
            return entities.stream()
                    .map(this::toDto)
                    .collect(Collectors.toList());
        }

        @Override
        public Customer toEntity(CustomerRequest dto) {
            return new Customer(dto.getName(), dto.getAge());
        }

        @Override
        public List<Customer> toEntity(List<CustomerRequest> dtos) {
            return dtos.stream()
                    .map(this::toEntity)
                    .collect(Collectors.toList());
        }
    };
}
