package com.semenov.jskool.payload;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerRequest {
    private String name;
    private Integer age;
}
