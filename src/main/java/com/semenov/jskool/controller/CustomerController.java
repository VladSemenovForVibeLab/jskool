package com.semenov.jskool.controller;

import com.semenov.jskool.entity.Customer;
import com.semenov.jskool.mappers.CustomerMapper;
import com.semenov.jskool.mappers.Mappable;
import com.semenov.jskool.payload.CustomerRequest;
import com.semenov.jskool.service.interf.CRUDService;
import com.semenov.jskool.service.interf.CustomerService;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.mbeans.UserMBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(CustomerController.CUSTOMER_REST_URL)
public class CustomerController extends CRUDRestController<Customer,Long,CustomerRequest> {
    public static final String CUSTOMER_REST_URL="/api/v1";
    private final CustomerService customerService;
    @Override
    CRUDService<Customer, Long,CustomerRequest> getService() {
        return customerService;
    }
}
