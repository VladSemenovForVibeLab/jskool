package com.semenov.jskool.controller;

import com.semenov.jskool.payload.MessageResponse;
import com.semenov.jskool.service.interf.CRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class CRUDRestController<E,K,T> {
    abstract CRUDService<E,K,T> getService();

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> createAdd(@RequestBody T request){
        try{
            getService().add(request);
            return new ResponseEntity<>(new MessageResponse(request.toString(), HttpStatus.OK),HttpStatus.OK);
        }catch (Exception e){
            e.fillInStackTrace();
        }
        return new ResponseEntity<>(new MessageResponse("Something went wrong",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping("/update")
    public ResponseEntity<MessageResponse> update(@RequestBody E request){
        try {
            E entityRequest = getService().update(request);
            return new ResponseEntity<>(new MessageResponse("Updated",HttpStatus.OK),HttpStatus.OK);
        }catch (Exception e){
            e.fillInStackTrace();
        }
        return new ResponseEntity<>(new MessageResponse("Something went wrong",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @GetMapping("/all")
    public ResponseEntity<?> findAll(){
        try {
            List<T> objectsDto = getService().findAll();
            return new ResponseEntity<>(objectsDto,HttpStatus.OK);
        }catch (Exception e){
            e.fillInStackTrace();
        }
        return new ResponseEntity<>(new MessageResponse("Something went wrong",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @GetMapping("/get/{id}")
    public ResponseEntity<?> getById(@PathVariable K id){
        try {
            T objectDto = getService().findById(id);
            if(objectDto==null){
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(objectDto);
        }catch (Exception e){
            e.fillInStackTrace();
        }
        return new ResponseEntity<>(new MessageResponse("Something went wrong",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable K id){
        getService().delete(id);
        return ResponseEntity.noContent().build();
    }
}
