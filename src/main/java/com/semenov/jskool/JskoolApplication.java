package com.semenov.jskool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JskoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(JskoolApplication.class, args);
	}

}
