package com.semenov.jskool.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionBody {
    private String message;
    private Map<String,String> errors;

    public ExceptionBody(String message) {
        this.message = message;
    }

}
