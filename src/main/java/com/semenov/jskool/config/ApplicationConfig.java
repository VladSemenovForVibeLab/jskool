package com.semenov.jskool.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
    @Bean
    public OpenAPI openAPI(){
        return new OpenAPI()
                .info(new Info()
                        .title("JSKOOL API")
                        .description("Demo Spring Boot Application")
                        .version("1.0")
                        .contact(new Contact()
                                .name("Vladislav Semenov")
                                .email("ooovladislavchik@gmail.com")));

    }
}
