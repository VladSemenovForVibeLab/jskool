FROM maven:3.8.5-openjdk-17-slim AS build
WORKDIR /
COPY /src /src
COPY pom.xml /
RUN mvn -f /pom.xml clean package -DskipTests

FROM openjdk:17-jdk-slim
WORKDIR /
COPY /src /src
COPY --from=build /target/*.jar jskool.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","jskool.jar"]