# CRUD приложение на Spring Java с Docker и Docker Compose

## Описание
Данное CRUD приложение разработано на Spring Java и использует PostgreSQL или MariaDB в качестве базы данных. Также приложение может быть запущено в контейнерах Docker.

Приложение представляет собой простой REST API, который предоставляет возможность выполнять операции CRUD (Создание, Чтение, Обновление, Удаление) с сущностью Generic.

## Требования
- JDK 17
- Docker
- Docker Compose

## Установка и запуск
1. Склонируйте репозиторий:

```bash
git clone git@gitlab.com:VladSemenovForVibeLab/jskool.git
```

2. Перейдите в папку проекта:

```bash
cd jskool
```

3. Соберите и запустите контейнеры Docker:

```bash
docker-compose up -d
```

4. Подключитесь к базе данных PostgreSQL или MariaDB с помощью соответствующего клиента (например, psql или mysql).

5. Создайте базу данных:

Для PostgreSQL:
```sql
CREATE DATABASE jskool_postgres_db;
```

Для MariaDB:
```sql
CREATE DATABASE jskool_maria_db;
```

6. Соберите и запустите приложение:

```bash
./mvnw spring-boot:run
```

## Использование

После успешного запуска приложения вы можете использовать следующие эндпоинты для выполнения операций CRUD:

### Создание сущности
**URL**: `POST /api/v1/add`

**Пример запроса**:
```json
{
  "name": "John Doe",
  "age": 30
}
```

### Получение всех сущностей
**URL**: `GET /api/v1/all`

**Пример ответа**:
```json
[
  {
    "id": 1,
    "name": "John Doe",
    "age": 30
  },
  {
    "id": 2,
    "name": "Jane Smith",
    "age": 25
  }
]
```

### Получение сущности по ID
**URL**: `GET /api/v1/get/{id}`

**Пример ответа**:
```json
{
  "id": 1,
  "name": "John Doe",
  "age": 30
}
```

### Обновление сущности
**URL**: `PUT /api/v1/update/{id}`

**Пример запроса**:
```json
{
  "name": "Updated Name",
  "age": 40
}
```

### Удаление сущности
**URL**: `DELETE /api/generic/{id}`

## Конфигурация

Для настройки приложения вы можете изменить следующие свойства в файле `src/main/resources/application.properties`:

```properties
# Настройки базы данных PostgreSQL
spring.datasource.url=jdbc:postgresql://localhost:5432/jskool_postgres_db
spring.datasource.username=postgres
spring.datasource.password=postgres

# Настройки базы данных MariaDB
#spring.datasource.url=jdbc:mariadb://localhost:3306/jskool_maria_db
#spring.datasource.username=root
#spring.datasource.password=root

# Порт, на котором приложение будет запущено
server.port=8080